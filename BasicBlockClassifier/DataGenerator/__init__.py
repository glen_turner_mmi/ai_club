# CLASS TO GENERATE DATA

import numpy as np
import matplotlib.pyplot as plt
import random
import math
import time
import tensorflow as tf
import time

class DataGenerator:
    
    def __init__(self, shape, colours, noise_mean = 0, noise_stdev = 0, mode = "simple"):
        self.shape=shape
        self.colours=colours
        self.noise_mean = noise_mean
        self.noise_stdev = noise_stdev
        self.mode = mode
        
        self.min_height = int(self.shape[0] * 0.1) + 1
        self.max_height = int(self.shape[0] * 0.8) + 1
        self.min_width = int(self.shape[0] * 0.1) + 1
        self.max_width = int(self.shape[0] * 0.8) + 1
        self.label_format = "[ object not present, color_labels... ]" #, x ,  y, height, width, color_label ]"
        
        print ("label format = {}".format(self.label_format))

    def get_batch_single_thread(self, num):
        labels = []
        images = []
        previous_time = time.time()
        has_printed = False
        
        for i in range(num):
            now_time = time.time()
            if now_time - previous_time > 0.5:
                if has_printed == False:
                    print("Generating batch.")
                    has_printed = True
                previous_time = now_time
                print("{} % done.".format((i / num) * 100.0))
                
            img, label = self.make_image()
            labels.append(label)
            images.append(img)
        return images, np.array(labels).astype(float)
    
    def get_batch(self, num):
        return self.get_batch_single_thread(num)

    
    
    def make_image(self):
        image = np.zeros(shape = self.shape)
        if(random.randint(0,1) == 1): # 50/50 chance of returning a blank image
            colour_index = random.randint(0,len(self.colours) -1 )
            
            height = random.randint(self.min_height, self.max_height)
            width = random.randint(self.min_width, self.max_width)
            x = random.randint(0,self.shape[0] - width)
            y = random.randint(0,self.shape[1] - height)

            for i in range(3):
                image[x: x + width, y : y + height , i] = self.colours[colour_index][i]
                colour_labels = np.zeros(len(self.colours), dtype=int)
                colour_labels[colour_index] = 1
                # label = np.array([0, 1, x/self.shape[0] , y/self.shape[1], height / self.shape[0], width / self.shape[1], *colour_labels ]).astype(float)
                label = np.array([1, *colour_labels]).astype(float)
        else:
            x = 0
            y = 0
            height = 0
            width = 0
            colour_labels = np.zeros(len(self.colours), dtype=int)
            # label = np.array([1, 0, x , y, height, width, *colour_labels]).astype(float)
            label = np.array([0, *colour_labels]).astype(float)
            
        noise = np.random.normal(loc=self.noise_mean, size = self.shape, scale = self.noise_stdev)
        image = image + noise
        image = np.clip(image, 0, 1 )
        self.label_shape = label.shape
        
        return image, label